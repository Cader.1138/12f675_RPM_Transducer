/*
 * File:   main.c
 * Author: Charles Ader
 * Target: PIC12F675
 * Compiler: XC8 v2.45
 * IDE: MPLABX v6.20
 *
 * Created on June 15, 2024, 2:19 PM
 * 
 *                         PIC12F675
 *                 +----------:_:----------+
 *       PWR ->  1 : VDD               VSS : 8 <> GND
 *           <>  2 : RA5/OSC1      PGD/RA0 : 7 <> PGD/PULSE_IN
 *           <>  3 : RA4/OSC2      PGC/RA1 : 6 <> PGC/RELAY_OUT
 *       VPP ->  4 : RA3/MCLRn     INT/RA2 : 5 <> 
 *                 +-----------------------:
 *                          DIP-8
 * Description:
 * 
 *  Input (PULSE_IN) is a pulse with a duty cycled of at least 200 microseconds,
 *  and a period between 60milliseconds(17Hz) and 10milliseconds(100Hz).
 * 
 *  The period of PULSE_IN is measured between the low-to-high edges.
 * 
 *  The output (RELAY_OUT) drives the relay control circuit.
 * 
 *  RELAY_OUT is set low when the PULSE_IN period is longer than 14.814ms (67.5Hz)
 *
 *  RELAY_OUT is set high  when the PULSE_IN period is shorter than 13.157ms (76.0Hz)
 *
 * See:  https://forum.microchip.com/s/topic/a5CV40000001SOrMAM/t396511
 *
 * Notes:
 *  1973 PORSCHE 911 RPM TRANSDUCER 0 336 611 001
 *  0 336 611 006 = OFF(1,500 RPM) ON (1,610 RPM) 0.5amp = PORSCHE 901 615 113 00 "dashpot" relay for emission controlled cars with carbs.
 *  0 333 400 001 = OFF(1,350 RPM) ON (1,520 RPM) 2.5amp = PORSCHE 901 615 111 00 "MFI speed control" relay (supercedes Bosch 0 336 611 001)
 * 
 */

/* These are the trip points when the relay switched */
#define RELAY_ON_RPM  (1520ul)  /* 1,520 RPM when relay is switched from off to on */
#define RELAY_OFF_RPM (1350ul)  /* 1,350 RPM when relay is switched from on to off */

/* Define the system oscillator this code will setup */
#define _XTAL_FREQ (4000000ul)
#define FCY (_XTAL_FREQ/4ul)

#pragma config FOSC = INTRCIO   // Oscillator Selection bits (INTOSC oscillator: I/O function on GP4/OSC2/CLKOUT pin, I/O function on GP5/OSC1/CLKIN)
#pragma config WDTE = ON        // Watchdog Timer Enable bit (WDT enabled)
#pragma config PWRTE = OFF      // Power-Up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = OFF      // GP3/MCLR pin function select (GP3/MCLR pin function is digital I/O, MCLR internally tied to VDD)
#pragma config BOREN = ON       // Brown-out Detect Enable bit (BOD enabled)
#pragma config CP = OFF         // Code Protection bit (Program Memory code protection is disabled)
#pragma config CPD = OFF        // Data Code Protection bit (Data memory code protection is disabled)

/* Include target specific definitions for Special Function Registers */
#include <xc.h>
#include <stdint.h>

/*
 * Interrupt handler
 */
void __interrupt() ISR(void)
{
    /* This application does not use interrupts */
    for(;;); /* loop and force a WDT reset */
}
#define GPIO_IN   1
#define GPIO_OUT  0


#define PULSE_IN  GPIObits.GP0
#define RELAY_OUT GPIObits.GP1
#define PULSE_DIR TRISIObits.TRISIO0
#define RELAY_DIR TRISIObits.TRISIO1


#define RELAY_OFF 0
#define RELAY_ON  1

#define T1_PRESCALE (1)
#define T1_CLOCKS_PER_SEC  (FCY/T1_PRESCALE)
#define T1_COUNTS_PER_TICK (T1_CLOCKS_PER_SEC/50ul)         /* 50Hz or 20ms */
#define T1_TICKS_UNTIL_INPUT_TIMEOUT (3ul)                  /* 60ms */
#define VALID_PERIOD_MIN (T1_CLOCKS_PER_SEC/1000ul)         /* 1000Hz or 1ms or 20000 RPM */

/* used to convert units */
#define RPM_TO_RPS (60ul)
#define INPUT_PULSES_PER_REV (3ul)

/* do math for trip point constants */
#define FAST_TRIP_POINT    ((T1_CLOCKS_PER_SEC*RPM_TO_RPS)/(INPUT_PULSES_PER_REV*RELAY_ON_RPM))
#define SLOW_TRIP_POINT    ((T1_CLOCKS_PER_SEC*RPM_TO_RPS)/(INPUT_PULSES_PER_REV*RELAY_OFF_RPM))

#if ((T1_COUNTS_PER_TICK * T1_TICKS_UNTIL_INPUT_TIMEOUT) > 65535ul)
#error TIMER1 clock rate too fast
#endif

/*
 * Initialize this PIC
 */
void Init_PIC(void)
{
    uint16_t SpinWait;
    
    INTCON = 0;
    VRCON = 0;      /* Disable voltage reference */

    ADCON0 = 0;     /* Disable ADC */
    ANSEL = 0;      /* Disable ADC analog inputs */
    TRISIO = 0xFF;
    CMCON = 0x07;   /* Disable analog Comparator inputs */
    GPIO = 0;

    
    /* give the In-Circuit-Debug tool a chance before making PGC an output */
    for(SpinWait = 200; SpinWait; --SpinWait)
    {
        CLRWDT();
        __delay_ms(1);
    }
    CLRWDT();
    
    /* Make RELAY_OUT an output bit */
    RELAY_DIR = GPIO_OUT;
    
    /*
     * Initialize TIMER1
     */
    PIE1bits.TMR1IE = 0;
    T1CON = 0;      /* clock source Fosc/4 */
    
#if (T1_PRESCALE == 1)
    /* prescale 1:1 is the default */
#elif (T1_PRESCALE == 2)
    /* prescale 1:2 */
    T1CONbits.T1CKPS = 1;
#elif (T1_PRESCALE == 4)
    /* prescale 1:4 */
    T1CONbits.T1CKPS = 2;
#elif (T1_PRESCALE == 8)
    /* prescale 1:8 */
    T1CONbits.T1CKPS = 3;
#else
#error TIMER1 prescale not correct
#endif
    
    TMR1H = 0;
    TMR1L = 0;
    T1CONbits.TMR1ON = 1;
}

/*
 * Read TIMER1
 * 
 * TIMER1 input clock is (Fosc/4) / T1_PRESCALE
 * 
 * Rollover at 65536 counts
 */
uint16_t Read_TIMER1(void)
{
    union {
        uint16_t word;
        struct {
            uint8_t lo;
            uint8_t hi;
        };
    } Result;
    
    Result.lo = TMR1L;
    Result.hi = TMR1H;
    if ((Result.lo ^ TMR1L) & 0x80)
    {
        Result.lo = TMR1L;
        Result.hi = TMR1H;
    }

    return Result.word;
}
/*
 * Global data
 */
uint16_t TickSample;
uint16_t Period_start;
uint16_t Period_T1counts;
uint16_t TickCount;

union {
    uint8_t flags;
    struct {
        unsigned LowSpeed       : 1;
        unsigned PeriodValid    : 1;
        unsigned SensorState    : 1;
        unsigned RelayOn        : 1;
    };
} RPM_Transducer;
uint8_t EdgeEventTimeoutCount;

/*
 * Main application
 */
void main(void) 
{
    /*
     * Application initialization
     */
    Init_PIC();
    
    RELAY_OUT = RELAY_OFF;
    RPM_Transducer.flags = 0;
    TickCount = Read_TIMER1();
    Period_T1counts = UINT16_MAX;
    EdgeEventTimeoutCount = 0;
    RPM_Transducer.LowSpeed = 1;

    /*
     * Application process loop
     */
    while(1)
    {
        CLRWDT();
        
        /* find out if 20 milliseconds has passed */
        TickSample = Read_TIMER1();
        if((TickSample - TickCount) >= T1_COUNTS_PER_TICK)
        {
            TickCount += T1_COUNTS_PER_TICK;
            if(EdgeEventTimeoutCount) 
            {
                EdgeEventTimeoutCount = EdgeEventTimeoutCount - 1;
            }
            else
            {
                /* crank speed to slow to measure if no low to high edge for 60 to 80 milliseconds */
                Period_T1counts = UINT16_MAX;   /* crank speed too slow to measure */
                RPM_Transducer.LowSpeed = 1;    /* assert RPM is below 1000 */
                RPM_Transducer.PeriodValid = 0; /* assert crank period is unknown */
                RPM_Transducer.RelayOn = 0;     /* Turn off relay */
                RELAY_OUT = RELAY_OFF;
            }
        }
        
        if(PULSE_IN != RPM_Transducer.SensorState) 
        {
            RPM_Transducer.SensorState ^= 1;    /* crank sensor changed */
            if(RPM_Transducer.SensorState)      /* and it's the low to high edge */
            {
                EdgeEventTimeoutCount = T1_TICKS_UNTIL_INPUT_TIMEOUT;
                if(RPM_Transducer.LowSpeed)
                {
                    RPM_Transducer.LowSpeed = 0;
                    Period_start = TickSample;
                }
                else
                {
                    Period_T1counts = TickSample - Period_start;
                    Period_start = TickSample;
                    if(Period_T1counts > VALID_PERIOD_MIN)
                    {
                        RPM_Transducer.PeriodValid = 1;
                    }
                }
                
                if(RPM_Transducer.PeriodValid)
                {
                    if(!RPM_Transducer.RelayOn)
                    {
                        if (Period_T1counts < FAST_TRIP_POINT)  /* faster than 1520 RPM */
                        {
                            RPM_Transducer.RelayOn = 1;         /* Turn on relay */
                            RELAY_OUT = RELAY_ON;
                        }
                    }
                    else
                    {
                        if (Period_T1counts > SLOW_TRIP_POINT)  /* slower than 1350 RPM */
                        {
                            RPM_Transducer.RelayOn = 0;         /* Turn off relay */
                            RELAY_OUT = RELAY_OFF;
                        }
                    }
                }
            }
        }
    }
}
