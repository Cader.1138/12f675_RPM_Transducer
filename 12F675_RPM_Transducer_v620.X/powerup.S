;	XC8 PIC powerup routine
;
; This module may be modified to include custom code to be executed 
; immediately after reset. After performing whatever powerup code
; is required, it should jump to "start"

#include	"aspic.h"

	global	powerup,start
	psect	powerup,class=CODE,delta=2
powerup:
;
; Implement oscillator calibration method
; that does not lock up when the RETLW opcode
; gets erased when In-Circuit-Programming failed.
;
#if defined(_12F675) || defined(_16F630)
    global  ___osccal_val
    btfsc   STATUS,5
    goto    SkipCal
    bsf     STATUS,5
    call    ___osccal_val
    movwf   OSCCAL
SkipCal:
#endif
;
;	Insert special powerup code here
;
;


; Now lets jump to start 
#if	defined(_PIC14)
	clrf	STATUS
	movlw	start>>8
	movwf	PCLATH
	goto	start & 0x7FF | (powerup & not 0x7FF)
#endif
#if	defined(_PIC14E) || defined(_PIC14EX)
	clrf	STATUS
	movlp	start>>8
	goto	start & 0x7FF | (powerup & not 0x7FF)
#endif
#if	defined(_PIC16)
	movlw	start>>8
	movwf	PCLATH
	movlw	start & 0xFF
	movwf	PCL
#endif
	end
