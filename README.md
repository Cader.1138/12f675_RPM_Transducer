PIC12F675 Porsche RPM Transducer
====================================

This is source code based on a preliminary project description found on the Microchip forum.

See this topic: https://forum.microchip.com/s/topic/a5CV40000001SOrMAM/t396511

                         PIC12F675
                 +----------:_:----------+
       PWR ->  1 : VDD               VSS : 8 <> GND
           <>  2 : RA5           PGD/RA0 : 7 <> PGD/PULSE_IN
           <>  3 : RA4           PGC/RA1 : 6 <> PGC/RELAY_OUT
       VPP <>  4 : RA3/MCLRn     INT/RA2 : 5 <> 
                 +-----------------------:
                          DIP-14
Description:

 Input (PULSE_IN) is a pulse with a duty cycled of at least 200 microseconds,
 and a period between 60milliseconds(17Hz) and 10milliseconds(100Hz).

 The period of PULSE_IN is measured between the low-to-high edges.

 The output (RELAY_OUT) drives the relay control circuit.

 RELAY_OUT is set low when the PULSE_IN period is longer than 44.446ms.

 RELAY_OUT is set high  when the PULSE_IN period is shorter than 39.47ms.
